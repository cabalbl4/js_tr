var global = this;
if(typeof global._i18n_lang == 'undefined')  global._i18n_lang = {}; 

global._i18n_lang.de = {
  desc: {
    fullname: "Deutstch",
    shortname: "de"  
  }, 
  data : {
"nonexistent_test.gif" : ["nonexistent_test_de.gif"],
  "hello world" : [ "guten tag, welt" ],
  "registar": [ "registar", "registars"], 
  "tvpanel": [ "tv-Panel", "tv-Panele"], 
  "ipanel": [ "selbst. panel", "selbst. panels"],
  "operator": [ "betreiber", "betreibers"], 
  "Operator": [ "Betreiber", "Betreibers"], 
  "Select language": [ "Sprache wählen"],
  "Select theme":  ["Theme wählen"],
  "Please select component" : ["Bitte wählen Sie einen komponent"],
  "admin" : ["verwalter"],
  "Administrator" : ["Verwalter"],
  "Current client":["Aktuelle kunde"],
  "Operator panel":["Betreiber-panel"],
  "Served":["Serviert"],
  "Rejected" : ["Verzicht"],
  "Route forward": ["Weiter senden"],
  "Settings" :["Einstellungen"],
  "Other options" : ["Andere Optionen"],
  "Other settings" : ["Andere Einstellungen"],
  "Admin panel" : ["Verwalter-panel"],
  "Service" : ["Bedienung", "Bedienungen"],
  "Schedule" : ["Zeitplan", "Zeitplane"],
  "User" : ["Benutzer", "Benutzer"],
  "Please enter user name and password": ["Bitte geben Sie Benutzername und Passwort"],
  "login/password incorrect" : ["Name oder Passwort inkorrekt"],
   "Password": ["Passwort"],
   "Role" : ["Role","Rolen"],
   "Apply changes": [ "Änderungen übernehmen" ],
   "Delete": ["Löschen"],
   "Set password" : ["Passwort setzen"],
   "Create user" : ["Benutzer schaffen"],
   "User name" : ["Benutzer name"],
   "Cancel" : ["Abbrechen"],
   "Ok" : ["Ok"],
   "User name can not be empty":["Benutzername darf nicht leer sein"],
   "Remove user %s?" : ["Benutzer %s entfernen?"],
   "Change user %s password?": [ "Benutzer %s Passwort wechseln?" ],
   "Operation" : ["Operation", "Operationen"],
   "Short Name" : ["Kurzname"],
   "Full Name" : ["Vollständiger Name"],
   "Translated full Names" : [ "Übersetzt vollständige Namen"],
   "Translated short Names" : [ "Übersetzt kurze Namen" ],
   "Language" : ["Sprache", "Sprachen"],
   "Default" : ["Voreinstellung"],
   "Optional settings (may be unset)" : [ "Optionale Einstellungen (darf ungesetzt sein)" ],
   "Languages, other than default, can be set here if they are used in multilingual queue. If the queue is single-lingual, this settings can reamin unset" : 
   ["Sprachen, die nicht standardmäßig sind, kann hier eingestellt werden, wenn sie in mehrsprachigen Warteschlange verwendet wird. Wenn die Warteschlange Single-lingual ist, können diese Einstellungen uneingestellt sein"],
   "Average time for operation" : ["Durchschnittliche Zeit für die Operation"],
   "Maximum time for operation" : ["Maximale Zeit für die Operation"],
   "Min." : ["Min."],
   "Add operation" : [  "Operation schaffen"  ],
   "Delete operation" : ["Operation löschen" ],
   "Add translation" : ["Übersetzung hinzufügen"],
   "Save" : ["Speichern"],
   "Add service" : ["Bedienung hinzufügen"],
   "Delete service" : ["Bedienung löschen"],
   "Add service group" : ["Bedienung-Gruppe hinzufügen"]

  }
};
