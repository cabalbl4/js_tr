/* 
 * JS_TR
 * By Vokhmin Ivan <cabalbl4@gmail.com> - https://github.com/Cabalbl4/js_tr
 * Released under the MIT license
 */
var global = this;
if(typeof global._i18n_lang == 'undefined')  global._i18n_lang = {};
global._i18n_tr_ = new function() {
  var self = this;
  
  /* list of current languages*/
  self.langs =  global._i18n_lang;
  
  var m_lang = null;
  
  /* Use this property to change current language*/
  Object.defineProperty(this, "currentLang", {
   get: function() {
     return m_lang;
   },
   set: function(_langName) {
     if( ! self.langs[_langName] ) throw "no such language: "+_langName;
     m_lang = _langName;
     self.applyLanguage();
   }
   
  });
  
  self.selectors = ['tr', 'data-tr'];
  self.idxSelectors = ['trform', 'data-trform']; 
  self.attrSelectors = ['atr', 'data-atr'];
  //Add current language to lang attribute of translated element (See W3C lang)
  self.supportLangTag = false;
  
  var l_onTranslationListeners = [];
  self.applyLanguage = function() {
    if(! m_lang ) return;
    //tr innerHTML
    var l_elems = [];
	for(var i=0; i<self.selectors.length; i++) {
		l_elems = l_elems.concat(Array.prototype.slice.call(document.querySelectorAll('['+self.selectors[i]+']')));
	}
    for (var i=0; i<l_elems.length; i++) {
      var l_text = null;
      var l_selector = null;	
	for(var j=0; j<self.selectors.length; j++) {	
		if(! l_elems[i].hasAttribute(self.selectors[j]) ) {
			continue;
		}
		l_selector = self.selectors[j];
		l_text=l_elems[i].getAttribute(self.selectors[j]);
		break;
	}
      //Empty tr selector
      if(l_text == "") {
        l_text = l_elems[i].innerHTML;
        l_elems[i].setAttribute(l_selector, l_text);
      };
     var l_idxSelector = null;
	for(var j=0; j<self.idxSelectors.length; j++) {
		if(l_elems[i].hasAttribute(self.idxSelectors[j])) {
			l_idxSelector = l_elems[i].getAttribute(self.idxSelectors[j]);
			break;
		}
	}


      var l_index = l_idxSelector && (isNaN(parseInt(l_idxSelector))) ?  parseInt(l_idxSelector) : 1;
	
      l_elems[i].innerHTML = self.tr(l_text, l_index);
      if(self.supportLangTag) {
      	l_elems[i].setAttribute("lang", m_lang);
      }
  
    };

	//Tr attributes
	var l_attrElems = [];
	for(var i=0; i<self.attrSelectors.length; i++) {
			l_attrElems = l_attrElems.concat(
				Array.prototype.slice.call(document.querySelectorAll('['+self.attrSelectors[i]+']'))
				);
		}
	
	for (var i=0; i<l_attrElems.length; i++) {
		var l_attrTr = null;
		for(var k=0; k<self.attrSelectors.length; k++) {	
		if(! l_attrElems[i].hasAttribute(self.attrSelectors[i]) ) {
			continue;
		}
			l_attrTr = l_elems[i].getAttribute(self.attrSelectors[i]);
			break;
		}
		
		
		if(! l_attrTr) continue;
		var l_attrArr = l_attrTr.replace(/\s/g, '').split(",");
		if(! l_attrElems[i]["__tr_attrcache__"] ) {
			l_attrElems[i]["__tr_attrcache__"] = {};
			for(var j=0; j<l_attrArr.length; j++) {
				l_attrElems[i]["__tr_attrcache__"][l_attrArr[j]] = l_attrElems[i].getAttribute(l_attrArr[j]);
			};
		};
		for(var j=0; j<l_attrArr.length; j++) {
			if(l_attrElems[i]["__tr_attrcache__"][l_attrArr[j]]) {
				l_attrElems[i].setAttribute(l_attrArr[j], self.tr( l_attrElems[i]["__tr_attrcache__"][l_attrArr[j]] ) );
			};
		};
		
		

	};
    
    for(var i=0; i<l_onTranslationListeners.length; i++) {
    	l_onTranslationListeners[i](m_lang);
    };
    
  };
  
  //Global onTranslation listener functions
  self.addOnTranslationListener = function(_func) {
  	l_onTranslationListeners.push(_func);
  };
  
  self.removeOnTranslationListener = function(_func) {
  	for(var i=l_onTranslationListeners.length; i>=0; i--) {
	  	 if(_func ==  l_onTranslationListeners[i]) {
	  	 	l_onTranslationListeners.splice(i, 1);
	  	 }	
  	};
  
  };
  
  self.tr = function(_text, _formIndex) {
    if(! m_lang) return _text;
    if(! self.langs[m_lang]) return _text;
     if(! self.langs[m_lang].data) return _text;
    var l_formIndex = _formIndex?_formIndex : 1;
    if( ! self.langs[m_lang].data[_text] ) return _text;
    if( ! self.langs[m_lang].data[_text][l_formIndex - 1] ) {
        if(self.langs[m_lang].data[_text][0]) return self.langs[m_lang].data[_text][0];
        return _text;
    };
    return self.langs[m_lang].data[_text][l_formIndex - 1];
  };
  
  
  self.xmlRequestor = function() {
            var xmlhttp;
            try {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                xmlhttp = false;
                }
            }
            if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
                xmlhttp = new XMLHttpRequest();
            }
            return xmlhttp;  
        };
  
};



global.tr = global._i18n_tr_.tr;
global.addOnTranslationListener = global._i18n_tr_.addOnTranslationListener;
global.removeOnTranslationListener = global._i18n_tr_.removeOnTranslationListener;
global.currentLang = function() { return global._i18n_tr_.currentLang; }

/**
 * Set current language by code
 * @function setLanguage
 * @param {strng} _lang - language code
 */
global.setLanguage = function(_lang) {
  global._i18n_tr_.currentLang = _lang;
};

/**
 * Dynamically load translation file
 * WARNING. This function will eval a js file with address in _translation variable
 * Be sure to load correct translation file
 * @function tr.loadExternalTranslation
 * @param {string} _translation address of translation file (*.js)
 * 
 * @return added <script> tag to head
 */
global.tr.loadExternalTranslation = function(_translation) {
	var l_script = document.createElement("script");
	l_script.type= 'text/javascript';
	l_script.src = _translation;
	document.head.appendChild(l_script);
	return l_script;
};

/**
 * Return an array of available translations
 * @function listLanguages
 */
global.listLanguages = function() {
  var l_result = {};
  for (var name in global._i18n_tr_.langs) {
    l_result[name] = global._i18n_tr_.langs[name].desc.fullname;
  };
  return l_result;
};


/**
 * Load language json in sync mode as short code alias
 * 
 * @function tr.loadJSONSyncAs
 * @param {string} _shortCode language short code, i.e. 'de', 'en', 'fr'
 * @param {string} _path path to json, ie "/langs/data.json"
 * @throw DOMException
 */
global.tr.loadJSONSyncAs = function(_shortCode, _path) {
    var request = _i18n_tr_.xmlRequestor();
    request.open('GET', _path, false);
    request.send(null);
    if(request.status == 200) {
        var l_parsed = JSON.parse( request.responseText );
        var l_sname = _shortCode ? _shortCode : l_parsed.desc.shortname;
        _i18n_lang[l_sname] = l_parsed; 
        return true;
    } else {
        return request.status;
    };  
};

/**
 * Load language json in sync mode
 * 
 * @function tr.loadJSONSync
 * @param {string} _path path to json, ie "/langs/data.json"
 * @throw DOMException
 */
global.tr.loadJSONSync = function(_path) {
  return tr.loadJSONSyncAs(null, _path);
};

/**
 * Load language json in async mode as short code alias
 * 
 * @function tr.loadJSONSyncAs
 * @param {string} _shortCode language short code, i.e. 'de', 'en', 'fr'
 * @param {string} _path path to json, ie "/langs/data.json"
 * @param {function | null} _onReady called with the result
 * @throw DOMException
 */
global.tr.loadJSONAsyncAs = function(_shortCode, _path, _onReady) {
    var request = _i18n_tr_.xmlRequestor();
    request.open('GET', _path, true);
    request.onreadystatechange = function() {
      if(request.readyState != 4) return;
          if(request.status == 200) {
              var l_parsed =  JSON.parse( request.responseText );
              var l_sname = _shortCode ? _shortCode : l_parsed.desc.shortname;
                _i18n_lang[l_sname] = l_parsed;
                if(_onReady) _onReady(true);
            } else {
                if(_onReady) _onReady(request.status);
            }; 
          
        
    };
    request.send(null);
    return request;
};

/**
 * Load language json in async mode
 * 
 * @function tr.loadJSONSync
 * @param {string} _path path to json, ie "/langs/data.json"
 * @param {function | null} _onReady called with the result
 * @throw DOMException
 */

global.tr.loadJSONAsync = function( _path, _onReady) {
  return tr.loadJSONAsyncAs(null, _path, _onReady);  
    
};
