# js_tr
## A simple implementation of translation functions in JavaScript (Qt style)
### No dependencies, pure JS for web browsers

This tool is used to translate web pages in different languages with user-created translation files

### What is js_tr good for?
1. Translate a whole web-site or a standalone web-app on-the-fly, by calling a simple JavaScript function
2. Translate to any language you have a translation file for
3. Auto-generate and manage translation files with js-linguist tool ( https://github.com/Cabalbl4/js-linguist ), it can keep a record of all calls to js_tr in JavaScript or HTML (same way as Qt linguist is looking trough C++ code for tr calls). However, once js-linguist extracts text resources for translation, the rest of localization is up to user
4. Works for JS and HTML, in any JavaScript capable browser
5. Client-side translation. No server support needed
6. No external resources needed. Good for intranet pages/applications translation

#### This is a side-project of my queue-management system (freequeue), hope you find it useful.
#### License: MIT

## Enable

Just include js_tr.js from src folder and create language files in desired languages (see langs folder inside src for examples)
```html
    MANDATORY:
    <script src="js_tr.js"></script>
    OPTIONAL:
    <script src="langs/js_tr_en.js"></script>
    <script src="langs/js_tr_ru.js"></script>
    <script src="langs/js_tr_de.js"></script>
```

Translation files can also be added at runtime, by calling *tr.loadExternalTranslation* function
```javascript
 tr.loadExternalTranslation("langs/js_tr_hu.js");
```
This function returns a script tag added to head, to monitor its loading use *onload* and *onreadystatechange* of returned element 

For JSON files loading, see [*JSON Loading*](#loading-json-dictionaries)

## HTML
Usage: in HTML, use tag "tr" and "trform", or use HTML5 compliant form: "data-tr", "data-trform". Be advised, that mixing those forms within one html tag is a bad idea.

#### Example:

```html
<div tr="Operator" trform="2">Operators</div>

Short form: innerHTML will be added to tr attr automatically:
<div tr trform="2">Operator</div> 
<div data-tr data-trform="2">Operator</div> 
(after calling "setLanguage" tr will be set to "Operator")

Shortest form:
<div tr>Sometext</div> 
or
<div data-tr>Sometext</div>


```
"tr" is a phrase to translate. "trform" is plural form index, can be omitted. Default index is 1. 
Translation will auto-replace innerHTML of element

#### Attribute translation

Use *atr* (or *data-atr*) attribute to input comma (,) separated list of attributes for translation.
This will find and translte all attributes mentioned.
Be advised, that this command ignores trform, and always uses first form.

```html

Calling setLanguage will change src and alt attribute to appropriate values in translation file.
<img src="nonexistent_test_gb.gif" alt="Operator" atr="alt,src">

```

## JavaScript:
### tr - translate phrase
*tr(phrase (string) , plural index (int) );*

*tr(phrase (string));*

#### Example:
```javascript
var stringPlural = tr("Operator",2);  //Returns "Operator" translation in current language (plural form) i.e. 2 Operators
var stringSingle = tr("Operator"); //Returns "Operator" translation in current language i.e. 1 Operator
```

###  listLanguages - list available languages

Lists available languages.
returns { "language short name" : "language full name" } object. Use short name to switch languages


### setLanguage( lang )

Sets current language for JavaScript and HTML. Auto-translate all elements.
lang is short name of language

To set a language on load it is recomended to put this function in a script tag before &lt;/body&gt;
```html
<body>
....
<script>
    setLanguage('en');
</script>
</body>
```
Alternative way is to call *setLanguage* in *document.body.onload* or similar methods.

### addOnTranslationListener(function) and removeOnTranslationListener(function)
Simply add a listener to the global translation event. Passed function will receive one argument - string with current language name

```javascript
// Make an alert of language short name on each translation change
var a = function(_lang) { alert(_lang); };
addOnTranslationListener(a);
// Remove this listener
removeOnTranslationListener(a);
```

### currentLang()
Returns a short code of currently set language.
If setLanguage was not used - null will be returned.

###Loading JSON dictionaries

#### Functions to load JSON dictionaries:
##### Asynchronous JSON loading
*tr.loadJSONAsync( _path, _onReady);*
##### Synchronous JSON loading
*tr.loadJSONSync( _path);*

Where *_path* is path to get, i.e. *langs/js_en_gb_test.json* 

In sync mode, result will be immediate. A true will be returned if success, error code will be returned if given by the server, in other cases exception will be thrown by XMLHTTPReqest. In async mode, on finish *_onReady* function is called if present. Async function returns request object to monitor if needed.

To load a language json to custom short name, use *tr.loadJSONAsyncAs* or *tr.loadJSONSyncAs*, which have additional first argument (string custom shortname);

Find example of JSON file in src/langs folder.



#### For more usage examples, see test.html inside src folder

## Tools
A Qt-like js-linguist localization tool to auto-generate language js files
It can auto-scan your projects to extract all cases of js_tr usage in HTML and JS files to language file, sync those changes and much more. Written in C++/Qt :)
https://github.com/Cabalbl4/js-linguist
